package com.student.marksheet.models;
import org.springframework.http.HttpStatus;

import java.io.Serializable;


public class ResponseModel implements Serializable {

    private Object object;
    private String status;
    private String message;

    public Object getObject() {
        return object;
    }
    public void setObject(Object object) {
        this.object = object;
    }
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }

    public static ResponseModel getInstance() {
        ResponseModel response = new ResponseModel();
        response.setStatus(HttpStatus.OK.toString());
        return response;
    }
}
