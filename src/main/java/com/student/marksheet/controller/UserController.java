package com.student.marksheet.controller;

import com.student.marksheet.models.ResponseModel;
import com.student.marksheet.models.UserModel;
import com.student.marksheet.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/getUser/{id}")
    public ResponseEntity<ResponseModel> getStudent(@PathVariable(value = "id") Integer id) {
        ResponseModel responseModel = userService.getUser(id);
        return new ResponseEntity<>(responseModel, HttpStatus.OK);
    }

    @PostMapping(path = "/login")
    public ResponseEntity<ResponseModel> login(@RequestBody UserModel userModel){
        ResponseModel responseModel = userService.login(userModel);
        return new ResponseEntity<>(responseModel,HttpStatus.OK);
    }

    @PostMapping(path = "/update")
    public ResponseEntity<ResponseModel> addUser(@RequestBody UserModel userModel) {
        ResponseModel responseModel = userService.updateUser(userModel);
        return new ResponseEntity<>(responseModel,HttpStatus.OK);
    }

    @PostMapping(path = "/register")
    public ResponseEntity<ResponseModel> register(@RequestBody UserModel userModel) {
        ResponseModel responseModel = userService.register(userModel);
        return new ResponseEntity<>(responseModel,HttpStatus.OK);
    }
}

