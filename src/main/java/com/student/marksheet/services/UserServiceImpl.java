package com.student.marksheet.services;

import com.student.marksheet.models.ResponseModel;
import com.student.marksheet.models.UserModel;
import com.student.marksheet.repository.UserRepository;
import com.student.marksheet.repository.UserRepositoryHelper;
import com.sun.org.apache.regexp.internal.RE;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.xml.ws.ServiceMode;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    UserRepositoryHelper userRepositoryHelper;

    @Override
    public ResponseModel register(UserModel userModel){
        ResponseModel responseModel = ResponseModel.getInstance();
        UserModel userExist = userRepositoryHelper.fetchObjectByParam(UserModel.class,"email_id",userModel.getEmailId());
        if(userExist == null){
            String encodedPassword = Base64.getEncoder().encodeToString(userModel.getPassword().getBytes());
            userModel.setPassword(encodedPassword);
            responseModel.setObject(userRepository.save(userModel));
            responseModel.setMessage("User Register successfully..");
        } else {
            responseModel.setMessage("Email id already present please use different email id");
        }
        responseModel.setStatus(HttpStatus.OK.toString());
        return responseModel;
    }

    @Override
    public ResponseModel getUser(int id) {
        ResponseModel responseModel = ResponseModel.getInstance();
        Optional<UserModel> userModel =  userRepository.findById(id);
        if(userModel.isPresent()){
            responseModel.setObject(userModel.get());
            responseModel.setMessage("SUCCESS");
            responseModel.setStatus(HttpStatus.OK.toString());
        }else{
            responseModel.setMessage("User not present..");
            responseModel.setStatus(HttpStatus.OK.toString());
        }
        return responseModel;
    }

    @Override
    public ResponseModel login(UserModel userModel) {
        ResponseModel responseModel = ResponseModel.getInstance();
        UserModel dbUser = userRepositoryHelper.fetchObjectByParam(UserModel.class,"email_id", userModel.getEmailId());
        if(dbUser == null){
            responseModel.setMessage("Please enter correct email id");
        }else if(userModel.getEmailId().equals(dbUser.getEmailId()) &&
                userModel.getPassword().equals(new String(Base64.getDecoder().decode(dbUser.getPassword())))){
            responseModel.setMessage("Login successful..");
            responseModel.setObject(dbUser);
        } else {
            responseModel.setMessage("Login Failed - wrong email id or password");
        }

        responseModel.setStatus(HttpStatus.OK.toString());
        return responseModel;
    }

    @Override
    public ResponseModel updateUser(UserModel userModel) {
        ResponseModel responseModel = ResponseModel.getInstance();
        responseModel.setObject(userRepository.save(userModel));
        responseModel.setMessage("User added Successfully....");
        responseModel.setStatus(HttpStatus.OK.toString());
        return responseModel;
    }


}


