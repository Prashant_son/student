package com.student.marksheet.services;

import com.student.marksheet.models.ResponseModel;
import com.student.marksheet.models.UserModel;

import java.util.List;

public interface UserService {

    public ResponseModel getUser(int id);

    public ResponseModel updateUser(UserModel userModel);

    public ResponseModel login(UserModel userModel);

    public ResponseModel register(UserModel userModel);
}
