package com.student.marksheet.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.io.Serializable;

@Component
public class UserRepositoryHelper implements Serializable {


    @Autowired
    private EntityManager entityManager;

    public <T> T fetchObjectByParam(Class<T> entity, String param, Object paramValue) {
        Query query = entityManager.createQuery("SELECT e FROM "+ entity.getName() + " e WHERE "+param+" = :value ORDER BY e.id DESC");
        query.setParameter("value", paramValue);
        query.setMaxResults(1);
        try {
            return (T) query.getSingleResult();
        } catch (NoResultException exc) {
            return null;
        }
    }

}
