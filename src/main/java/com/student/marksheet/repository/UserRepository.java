
package com.student.marksheet.repository;

import com.student.marksheet.models.UserModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<UserModel, Integer> {


}

